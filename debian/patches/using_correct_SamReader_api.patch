Description: using the correct SamReader api
Author: Pierre Gruet <pgt@debian.org>
Origin: https://sources.debian.org/src/fastqc/0.11.9+dfsg-4/debian/patches/htsjdk-api.patch
Forwarded: not-needed
Last-Update: 2020-11-06

--- a/goby-distribution/src/main/java/org/campagnelab/goby/modes/SAMComparisonMode.java
+++ b/goby-distribution/src/main/java/org/campagnelab/goby/modes/SAMComparisonMode.java
@@ -32,9 +32,7 @@
 import org.campagnelab.goby.util.dynoptions.RegisterThis;
 import htsjdk.samtools.ValidationStringency;
 import it.unimi.dsi.logging.ProgressLogger;
-import htsjdk.samtools.SAMFileReader;
-import htsjdk.samtools.SAMRecord;
-import htsjdk.samtools.SAMRecordIterator;
+import htsjdk.samtools.*;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 
@@ -284,12 +282,15 @@
         }
 
         System.out.println("Comparing source bam and destination bam");
-        final SAMFileReader sourceParser = new SAMFileReader(new FileInputStream(sourceBamFile));
-        final SAMFileReader destParser = new SAMFileReader(new FileInputStream(destBamFile));
+        final SamInputResource sirSource = SamInputResource.of(new FileInputStream(sourceBamFile));
+        final SamReaderFactory srfSource = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+        final SamInputResource sirDest = SamInputResource.of(new FileInputStream(destBamFile));
+        final SamReaderFactory srfDest = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader sourceParser = srfSource.open(sirSource);
+        final SamReader destParser = srfDest.open(sirDest);
         // We need to set the validation to silent because an incomplete file (if the source isn't the entire file)
         // we can see errors that wouldn't exist in a real conversion.
-        sourceParser.setValidationStringency(ValidationStringency.SILENT);
-        destParser.setValidationStringency(ValidationStringency.SILENT);
         final SAMRecordIterator sourceIterator = sourceParser.iterator();
         final SAMRecordIterator destIterator = destParser.iterator();
         AlignmentReaderImpl gobyReader = null;
--- a/goby-distribution/src/main/java/org/campagnelab/goby/modes/SAMToCompactMode.java
+++ b/goby-distribution/src/main/java/org/campagnelab/goby/modes/SAMToCompactMode.java
@@ -242,10 +242,11 @@
         final ProgressLogger progress = new ProgressLogger(LOG);
         progress.displayFreeMemory = true;
         // the following is required to set validation to SILENT before loading the header (done in the SAMFileReader constructor)
-        SAMFileReader.setDefaultValidationStringency(ValidationStringency.SILENT);
-
         final InputStream stream = "-".equals(inputFile) ? System.in : new FileInputStream(inputFile);
-        final SAMFileReader parser = new SAMFileReader(stream);
+        final SamInputResource sir = SamInputResource.of(stream);
+        final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
         // transfer read groups to Goby header:
         final SAMFileHeader samHeader = parser.getFileHeader();
         final IndexedIdentifier readGroups = new IndexedIdentifier();
--- a/goby-distribution/src/main/java/org/campagnelab/goby/modes/SAMToCompactOldMode.java
+++ b/goby-distribution/src/main/java/org/campagnelab/goby/modes/SAMToCompactOldMode.java
@@ -128,8 +128,10 @@
         final int[] readLengths = createReadLengthArray();
 
         final ProgressLogger progress = new ProgressLogger(LOG);
-        final SAMFileReader parser = new SAMFileReader(new File(inputFile));
-        parser.setValidationStringency(ValidationStringency.SILENT);
+        final SamInputResource sir = SamInputResource.of(new File(inputFile));
+        final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
 
         progress.start();
 
--- a/goby-distribution/src/main/java/org/campagnelab/goby/modes/SAMToCompactSamHelperMode.java
+++ b/goby-distribution/src/main/java/org/campagnelab/goby/modes/SAMToCompactSamHelperMode.java
@@ -231,10 +231,11 @@
         final ProgressLogger progress = new ProgressLogger(LOG);
         progress.displayFreeMemory = true;
         // the following is required to set validation to SILENT before loading the header (done in the SAMFileReader constructor)
-        SAMFileReader.setDefaultValidationStringency(ValidationStringency.SILENT);
-
         final InputStream stream = "-".equals(inputFile) ? System.in : new FileInputStream(inputFile);
-        final SAMFileReader parser = new SAMFileReader(stream);
+        final SamInputResource sir = SamInputResource.of(stream);
+        SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
         // transfer read groups to Goby header:
         final SAMFileHeader samHeader = parser.getFileHeader();
         final IndexedIdentifier readGroups = new IndexedIdentifier();
@@ -827,4 +828,4 @@
     }
 
 
-}
\ No newline at end of file
+}
--- a/goby-distribution/src/main/java/org/campagnelab/goby/modes/SamExtractReadsMode.java
+++ b/goby-distribution/src/main/java/org/campagnelab/goby/modes/SamExtractReadsMode.java
@@ -30,8 +30,7 @@
 import htsjdk.samtools.ValidationStringency;
 import it.unimi.dsi.lang.MutableString;
 import it.unimi.dsi.logging.ProgressLogger;
-import htsjdk.samtools.SAMFileReader;
-import htsjdk.samtools.SAMRecord;
+import htsjdk.samtools.*;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 
@@ -142,8 +141,10 @@
         try {
             final ProgressLogger progress = new ProgressLogger(LOG);
             // the following is required to set validation to SILENT before loading the header (done in the SAMFileReader constructor)
-            SAMFileReader.setDefaultValidationStringency(ValidationStringency.SILENT);
-            final SAMFileReader parser = new SAMFileReader(new File(inputFilename), null);
+            final SamInputResource sir = SamInputResource.of(new File(inputFilename));
+            final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+            final SamReader parser = srf.open(sir);
 
             progress.start();
 
--- a/goby-distribution/src/main/java/org/campagnelab/goby/modes/SampleQualityScoresMode.java
+++ b/goby-distribution/src/main/java/org/campagnelab/goby/modes/SampleQualityScoresMode.java
@@ -29,8 +29,7 @@
 import it.unimi.dsi.fastutil.ints.IntArrayList;
 import it.unimi.dsi.fastutil.ints.IntList;
 import it.unimi.dsi.lang.MutableString;
-import htsjdk.samtools.SAMFileReader;
-import htsjdk.samtools.SAMRecord;
+import htsjdk.samtools.*;
 import org.apache.commons.lang.ArrayUtils;
 import org.apache.commons.logging.Log;
 import org.apache.commons.logging.LogFactory;
@@ -350,8 +349,10 @@
     private int processSamReadsFile(final String inputFilename) throws IOException {
         // Create directory for output file if it doesn't already exist
         int i = 0;
-        SAMFileReader.setDefaultValidationStringency(ValidationStringency.SILENT);
-        final SAMFileReader parser = new SAMFileReader(new FileInputStream(inputFilename));
+        final SamInputResource sir = SamInputResource.of(new FileInputStream(inputFilename));
+        final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
         for (final SAMRecord samRecord : new SAMRecordIterable(parser.iterator())) {
             final String quality = samRecord.getBaseQualityString();
             if (quality != null && quality.length() != 0) {
--- a/goby-distribution/src/test/java/org/campagnelab/goby/readers/sam/RoundTripAlignment.java
+++ b/goby-distribution/src/test/java/org/campagnelab/goby/readers/sam/RoundTripAlignment.java
@@ -32,9 +32,7 @@
 import org.campagnelab.goby.reads.RandomAccessSequenceTestSupport;
 import htsjdk.samtools.ValidationStringency;
 import it.unimi.dsi.logging.ProgressLogger;
-import htsjdk.samtools.SAMFileReader;
-import htsjdk.samtools.SAMRecord;
-import htsjdk.samtools.SAMRecordIterator;
+import htsjdk.samtools.*;
 import org.apache.commons.io.FilenameUtils;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
@@ -172,12 +170,15 @@
         }
 
         LOG.info("Comparing source bam and destination bam");
-        final SAMFileReader sourceParser = new SAMFileReader(new FileInputStream(sourceBamFilename));
-        final SAMFileReader destParser = new SAMFileReader(new FileInputStream(destBamFilename));
+        final SamInputResource sirSource = SamInputResource.of(new FileInputStream(sourceBamFilename));
+        final SamReaderFactory srfSource = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+        final SamInputResource sirDest = SamInputResource.of(new FileInputStream(destBamFilename));
+        final SamReaderFactory srfDest = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader sourceParser = srfSource.open(sirSource);
+        final SamReader destParser = srfDest.open(sirDest);
         // We need to set the validation to silent because an incomplete file (if the source isn't the entire file)
         // we can see errors that wouldn't exist in a real conversion.
-        sourceParser.setValidationStringency(ValidationStringency.SILENT);
-        destParser.setValidationStringency(ValidationStringency.SILENT);
         final SAMRecordIterator sourceIterator = sourceParser.iterator();
         final SAMRecordIterator destIterator = destParser.iterator();
         AlignmentReaderImpl gobyReader = null;
--- a/goby-distribution/src/test/java/org/campagnelab/goby/readers/sam/TestSamRecordParser.java
+++ b/goby-distribution/src/test/java/org/campagnelab/goby/readers/sam/TestSamRecordParser.java
@@ -25,8 +25,7 @@
 import htsjdk.samtools.ValidationStringency;
 import it.unimi.dsi.lang.MutableString;
 import junit.framework.Assert;
-import htsjdk.samtools.SAMFileReader;
-import htsjdk.samtools.SAMRecord;
+import htsjdk.samtools.*;
 import org.apache.commons.io.FilenameUtils;
 import org.slf4j.Logger;
 import org.junit.BeforeClass;
@@ -65,8 +64,10 @@
     // like 9 no genome
     public void testSamToCompactTrickCase9NoGenome() throws IOException {
         final String inputFile = "test-data/splicedsamhelper/tricky-spliced-9.sam";
-        final SAMFileReader parser = new SAMFileReader(new FileInputStream(inputFile));
-        parser.setValidationStringency(ValidationStringency.SILENT);
+        final SamInputResource sir = SamInputResource.of(new FileInputStream(inputFile));
+        final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
         final SamRecordParser recordParser = new SamRecordParser();
         for (final SAMRecord samRecord : new SAMRecordIterable(parser.iterator())) {
 
@@ -84,8 +85,10 @@
     public void testSamToCompactTrickCase10NoGenome() throws IOException {
 
         final String inputFile = "test-data/splicedsamhelper/tricky-spliced-10.sam";
-        final SAMFileReader parser = new SAMFileReader(new FileInputStream(inputFile));
-        parser.setValidationStringency(ValidationStringency.SILENT);
+        final SamInputResource sir = SamInputResource.of(new FileInputStream(inputFile));
+        final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
         final SamRecordParser recordParser = new SamRecordParser();
         for (final SAMRecord samRecord : new SAMRecordIterable(parser.iterator())) {
 
@@ -103,8 +106,10 @@
     @Test
     public void testSamToCompactTrickCase11() throws IOException {
         final String inputFile = "test-data/splicedsamhelper/tricky-spliced-11.sam";
-        final SAMFileReader parser = new SAMFileReader(new FileInputStream(inputFile));
-        parser.setValidationStringency(ValidationStringency.SILENT);
+        final SamInputResource sir = SamInputResource.of(new FileInputStream(inputFile));
+        final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
         final SamRecordParser recordParser = new SamRecordParser();
         for (final SAMRecord samRecord : new SAMRecordIterable(parser.iterator())) {
 
@@ -121,8 +126,11 @@
     public void testSamToCompactTrickCase12NoGenome() throws IOException {
 
         final String inputFile = "test-data/splicedsamhelper/tricky-spliced-12.sam";
-        final SAMFileReader parser = new SAMFileReader(new FileInputStream(inputFile));
         final SamRecordParser recordParser = new SamRecordParser();
+        final SamInputResource sir = SamInputResource.of(new FileInputStream(inputFile));
+        final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
         for (final SAMRecord samRecord : new SAMRecordIterable(parser.iterator())) {
 
             final GobySamRecord gobySamRecord = recordParser.processRead(samRecord);
@@ -146,7 +154,10 @@
     public void testSamToCompactTrickCase13NoGenomeSoftClips() throws IOException {
 
         final String inputFile = "test-data/splicedsamhelper/tricky-spliced-13.sam";
-        final SAMFileReader parser = new SAMFileReader(new FileInputStream(inputFile));
+        final SamInputResource sir = SamInputResource.of(new FileInputStream(inputFile));
+        final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
         final SamRecordParser recordParser = new SamRecordParser();
         for (final SAMRecord samRecord : new SAMRecordIterable(parser.iterator())) {
 
@@ -179,8 +190,10 @@
     public void testSamToCompactTrickCase13SoftClipsWithGenome() throws IOException {
 
         final String inputFile = "test-data/splicedsamhelper/tricky-spliced-14.sam";
-        final SAMFileReader parser = new SAMFileReader(new FileInputStream(inputFile));
-        parser.setValidationStringency(ValidationStringency.SILENT);
+        final SamInputResource sir = SamInputResource.of(new FileInputStream(inputFile));
+        final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
 
         final SamRecordParser recordParser = new SamRecordParser();
 
@@ -209,7 +222,10 @@
     public void testSamToCompactTrickCase15NoGenomeThreeSplice() throws IOException {
 
         final String inputFile = "test-data/splicedsamhelper/tricky-spliced-15.sam";
-        final SAMFileReader parser = new SAMFileReader(new FileInputStream(inputFile));
+        final SamInputResource sir = SamInputResource.of(new FileInputStream(inputFile));
+        final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
         final SamRecordParser recordParser = new SamRecordParser();
         for (final SAMRecord samRecord : new SAMRecordIterable(parser.iterator())) {
             final GobySamRecord gobySamRecord = recordParser.processRead(samRecord);
@@ -247,8 +263,10 @@
     public void testSamToCompactTrickCase16() throws IOException {
 
         final String inputFile = "test-data/splicedsamhelper/tricky-spliced-16.sam";
-        final SAMFileReader parser = new SAMFileReader(new FileInputStream(inputFile));
-        parser.setValidationStringency(ValidationStringency.SILENT);
+        final SamInputResource sir = SamInputResource.of(new FileInputStream(inputFile));
+        final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
         final SamRecordParser recordParser = new SamRecordParser();
         for (final SAMRecord samRecord : new SAMRecordIterable(parser.iterator())) {
             final GobySamRecord gobySamRecord = recordParser.processRead(samRecord);
@@ -337,8 +355,10 @@
     @Test
     public void testSamToCompactTrickCase17() throws IOException {
         final String inputFile = "test-data/splicedsamhelper/tricky-spliced-17.sam";
-        final SAMFileReader parser = new SAMFileReader(new FileInputStream(inputFile));
-        parser.setValidationStringency(ValidationStringency.SILENT);
+        final SamInputResource sir = SamInputResource.of(new FileInputStream(inputFile));
+        final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
         final SamRecordParser recordParser = new SamRecordParser();
         for (final SAMRecord samRecord : new SAMRecordIterable(parser.iterator())) {
             final GobySamRecord gobySamRecord = recordParser.processRead(samRecord);
@@ -422,8 +442,10 @@
     @Test
     public void testDelNonSplice1() throws IOException {
         final String inputFile = "test-data/splicedsamhelper/del-nonsplice-1.sam";
-        final SAMFileReader parser = new SAMFileReader(new FileInputStream(inputFile));
-        parser.setValidationStringency(ValidationStringency.SILENT);
+        final SamInputResource sir = SamInputResource.of(new FileInputStream(inputFile));
+        final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
         final SamRecordParser recordParser = new SamRecordParser();
         for (final SAMRecord samRecord : new SAMRecordIterable(parser.iterator())) {
             final GobySamRecord gobySamRecord = recordParser.processRead(samRecord);
@@ -453,8 +475,10 @@
     @Test
     public void testLeftPadding1() throws IOException {
         final String inputFile = "test-data/splicedsamhelper/leftpad-nosplice-1.sam";
-        final SAMFileReader parser = new SAMFileReader(new FileInputStream(inputFile));
-        parser.setValidationStringency(ValidationStringency.SILENT);
+        final SamInputResource sir = SamInputResource.of(new FileInputStream(inputFile));
+        final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
         final SamRecordParser recordParser = new SamRecordParser();
         for (final SAMRecord samRecord : new SAMRecordIterable(parser.iterator())) {
             final GobySamRecord gobySamRecord = recordParser.processRead(samRecord);
@@ -491,8 +515,10 @@
     @Test
     public void testSeqVarReads() throws IOException {
         final String inputFile = "test-data/seq-var-test/seq-var-reads-gsnap.sam";
-        final SAMFileReader parser = new SAMFileReader(new FileInputStream(inputFile));
-        parser.setValidationStringency(ValidationStringency.SILENT);
+        final SamInputResource sir = SamInputResource.of(new FileInputStream(inputFile));
+        final SamReaderFactory srf = SamReaderFactory.makeDefault().validationStringency(ValidationStringency.SILENT);
+
+        final SamReader parser = srf.open(sir);
         final SamRecordParser recordParser = new SamRecordParser();
         final PositionToBasesMap<PerQueryAlignmentData> seqvarDataMap = TestIteratedSortedAlignment2.readSeqVarFile(
                 "test-data/seq-var-test/seq-var-reads-gsnap.seqvar");
