libgoby-java (3.3.1+dfsg2-11) unstable; urgency=medium

  * Raising Standards version to 4.7.0 (no change)
  * Restricting autopkgtest architecture to amd64 and arm64, where sra-sdk
    packages are installable
  * Build-depending on pkgconf instead of obsolete pkg-config

 -- Pierre Gruet <pgt@debian.org>  Tue, 14 May 2024 18:17:12 +0200

libgoby-java (3.3.1+dfsg2-10) unstable; urgency=medium

  * Fixing the clean target (Closes: #1045612)
  * Update standards version to 4.6.2, no changes needed.

 -- Pierre Gruet <pgt@debian.org>  Sun, 03 Dec 2023 08:03:09 +0100

libgoby-java (3.3.1+dfsg2-9) unstable; urgency=medium

  * Updating d/watch to follow the change in the Github layout
  * Use secure URI in Homepage field.
  * Adding a --add-opens argument to run the tests with OpenJDK17
  * Reworking autopkgtest to provide it with a temporary home directory

 -- Pierre Gruet <pgt@debian.org>  Sat, 19 Nov 2022 00:23:11 +0100

libgoby-java (3.3.1+dfsg2-8) unstable; urgency=medium

  * Raising Standards version to 4.6.1 (no change)
  * Omitting more flaky tests (Closes: #1013664)

 -- Pierre Gruet <pgt@debian.org>  Sun, 03 Jul 2022 14:51:44 +0200

libgoby-java (3.3.1+dfsg2-7) unstable; urgency=medium

  * Creating the test-results directory before running the tests

 -- Pierre Gruet <pgt@debian.org>  Thu, 23 Dec 2021 15:39:39 +0100

libgoby-java (3.3.1+dfsg2-6) unstable; urgency=medium

  * Adding --no-parallel flag to dh_auto_test to avoid concurrent access to
    some files during the tests (Closes: #1002155)

 -- Pierre Gruet <pgt@debian.org>  Thu, 23 Dec 2021 12:40:36 +0100

libgoby-java (3.3.1+dfsg2-5) unstable; urgency=medium

  * Adding jaxb-api.jar in the manifest of goby-distribution.jar and putting
    libjaxb-api-java as an explicit dependency of goby-java
  * Omitting a test that fails depending on the output of a random number
    generator.

 -- Pierre Gruet <pgt@debian.org>  Mon, 29 Nov 2021 20:21:31 +0100

libgoby-java (3.3.1+dfsg2-4) unstable; urgency=medium

  * Adding missing dependency libjaxb-api-java, both in d/control and in
    the POM file
  * Cleaning unused commented lines in d/rules
  * Removing libpcre3-dev, which is in fact not needed at all, from the
    list of dependencies (Closes: #1000043)
  * Adding a patch so that the C++ code uses pcre2 instead of pcre3, in case it
    would be useful one day

 -- Pierre Gruet <pgt@debian.org>  Fri, 26 Nov 2021 22:44:01 +0100

libgoby-java (3.3.1+dfsg2-3) unstable; urgency=medium

  * Adding default-jdk in the Depends of the 2nd autopkgtest

 -- Pierre Gruet <pgt@debian.org>  Sun, 07 Nov 2021 14:59:49 +0100

libgoby-java (3.3.1+dfsg2-2) unstable; urgency=medium

  * Source-only upload
  * Raising Standards version to 4.6.0 (no change)

 -- Pierre Gruet <pgt@debian.org>  Sat, 06 Nov 2021 14:25:53 +0100

libgoby-java (3.3.1+dfsg2-1) unstable; urgency=medium

  * Initial release (Closes: #992044)

 -- Pierre Gruet <pgt@debian.org>  Wed, 18 Aug 2021 11:14:57 +0200
